var client = require('redis');

module.exports = (function(){
	var _client = {};
	var _prefix = "user_apns_";
	return {
		init: function(port, host) {
			_client = client.createClient(port, host);

			this.__defineGetter__('client', function(){
				return _client;
			});

			return this;
		},
		setToken: function(userId, token) {
			_client.set(_prefix + userId, token);
		},
		getToken: function(userId, cb) {
			return _client.get(_prefix + userId, cb);
		}
	};
})();
