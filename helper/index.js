module.exports = (function() {
	// Recursive validation
	var _checkValue = function(value, pattern, strict) {
		pattern = pattern || {
			dataType: "string"
		};
		var dt = pattern.dataType.split(':');
		switch (dt[0]) {
			case 'string':
				return (pattern.notNull) ? value.length > 0 : true;
			case 'object':
				var valid = true;
				for (var child in value) {
					if (child != 'dataType') {
						if (strict && !pattern.hasOwnProperty(child)) {
							return false;
						}
						valid = valid && _checkValue(value[child], pattern[child], strict);
					}
				}
				return valid;
				// to do: array
			case 'numeric':
				return /[0-9]$/g.test(value);
			case 'email':
				return /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/g.test(value) && value.length < 121;
			case 'enum':
				var values = dt[1].split('|');
				return values.indexOf(value) > -1;
			case 'username':
				return /^([a-zA-Z0-9_-]){3,20}$/g.test(value);
			case 'hashtag':
				return /^([a-zA-Z0-9_-]){3,60}$/g.test(value);
			case 'boolean':
				return /^(true|false)$/i.test(value);
			default:
				return false;
		}
	};
	var _setResponseHeaders = function(res) {
		res.header('Content-Type', 'application/json');
		res.header('Access-Control-Allow-Credentials', true);
		res.header('Access-Control-Allow-Origin', '*');
	};
	return {
		buildDatabaseConnectionString: function(config) {
			return config.protocol + '://' + config.username +
				':' + config.password + '@' +
				config.host + ':' + config.port + '/' + config.database;
		},
		throwApiError: function(res, code, message) {
			console.error('ERROR::' + code + ' => ' + message);
			_setResponseHeaders(res);
			res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
			res.status(400);
			res.end(JSON.stringify({
				error: {
					code: code,
					message: message
				}
			}));
		},
		sendData: function(res, data) {
			_setResponseHeaders(res);
			res.end(JSON.stringify({
				success: true,
				data: data
			}));
		},
		validate: function(data, pattern) {
			var valid = true;
			for (var r in data) {
				if (!pattern.hasOwnProperty(r)) {
					return false;
				}
				valid = valid && _checkValue(data[r], pattern[r], pattern[r].strictObject);
			}
			return valid;
		},
		mergeJson: function(original, replacement) {
			for (var item in replacement) {
				if (original.hasOwnProperty(item)) {
					if (typeof original[item] === 'object' && original[item] !== null) {
						original[item] = this.mergeJson(original[item], replacement[item]);
					} else {
						original[item] = replacement[item];
					}
				} else {
					original[item] = replacement[item];
				}
			}
			return original;
		},
		logArgs: function() {
			console.log(arguments);
		},
		formatToken: function(token) {
			var res = "<";
			token = token.toLowerCase();
			for(var i = 0; i < token.length; i+=8) {
				res += token.substr(i,8) + " ";
			}
			return res.substr(0, res.length -1) + ">";
		}
	};
})();