var apnagent = require('apnagent');
var path = require('path');
var redis = require('../redis');

module.exports = (function(){
	var _key = "", _cert = "";
	var _agent = {};
	var _notifLabels= {
		"follow_request": "{username} wants to follow you",
		"follow_request_confirmation": "Your friend, {username} has confirmed your following request",
		"new_private_message": "You have got a new message from {username}",
		"user_commented_on_post": "{username} has commented on your post",
		"user_cooled_on_post": "{username} has cooled your post",
		"friend_joined": "Your friend, {username} has joined to Jestr",
		"mentioned_you": "{username} has mentioned you in a comment",
        "started_following_you": "{username} has started following you"
	};
	return {
		init: function(key, cert) {

			_key = path.join(__dirname, key);
			_cert = path.join(__dirname, cert);

			_agent = new apnagent.Agent();
			_agent.set('key file', _key);
			_agent.set('cert file', _cert);

			_agent.enable('sandbox');

			this.__defineGetter__('agent', function(){
				return _agent;
			});
			_agent.connect(function (err) {
				if(err) {
					console.log('ERROR:' + err);
					return;
				}
				console.log('APNS Agent launched.');
				// test

				// redis.getToken(14, function(err, token) {
				// 	if(err) {
				// 		console.log('Failed to send test message');
				// 		return;
				// 	}
				// 	console.log(arguments);
				// 	var message =_agent.createMessage();
				// 	message.device(token);
				// 	message.alert('SZERVER ELINDULT!! #YOLO');
				// 	message.send();
				// });
			});

			return this;
		},
		sendNotificationToUser: function(userId, type, sender) {
			console.log(arguments);
			redis.getToken(userId, function(err, token) {
							console.log(arguments);

				if(err) {
					console.log('apns seems like DEAD');
					return;
				}
				var message = _agent.createMessage();
				message.device(token);

				var label = _notifLabels[type];
				label = label.replace('{username}', sender.username);
				message.sound('bingbong.aiff');
				message.alert(label);
				message.set('type', type);
				message.send();
			});
		}
	};
})();