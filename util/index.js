/**
 * Utils for image manipulation
 */

var im = require('imagemagick');
var fs = require('fs');
var util = require('util');
var s3 = require('node-s3');
/*
	
Access Key ID:
AKIAJK475ONJUNCILHKQ
Secret Access Key:
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

*/

module.exports = (function() {
	var _s3;
	var _profilePictureDimensions = {
		xsmall: {
			width: 30,
			height: 30
		},
		small: {
			width: 40,
			height: 40
		},
		medium: {
			width: 60,
			height: 60
		},
		large: {
			width: 130,
			height: 130
		},
		xlarge: {
			width: 220,
			height: 220
		},
		xxlarge: {
			width: 640,
			height: 640
		}
	};
	var _coverDimensions = {
		xsmall: {
			width: 30,
			height: 30
		},
		small: {
			width: 150,
			height: 50
		},
		medium: {
			width: 300,
			height: 100
		},
		large: {
			width: 300,
			height: 300
		},
		xlarge: {
			width: 600,
			height: 600
		},
		xxlarge: {
			width: 1500,
			height: 500
		}
	};
	var _postDimensions = {
		xsmall: {
			width: 30,
			height: 30
		},
		small: {
			width: 40,
			height: 40
		},
		medium: {
			width: 150,
			height: 150
		},
		large: {
			width: 300,
			height: 300
		},
		xlarge: {
			width: 502,
			height: 376
		}
	};
	var _cropImages = function(key, path, destinationFolder, pictures, dimensions, cb) {
		// ötszörös gyorsítás
		fs.rename(path, destinationFolder + '/' + pictures.orig, function(err) {
			if(err) {
				cb(err);
				return;
			}
			var args = [path],
				resizeTo;
			for (var p in dimensions) {
				resizeTo = dimensions[p].width + 'x' + dimensions[p].height;
				args = args.concat([
					'\(',
					'-clone', '0',
					'-resize', resizeTo + '^',
					'-gravity', 'Center',
					'-crop', resizeTo + '+0+0',
					'+repage',
					'-write', destinationFolder + '/' + pictures[p],
					'\)'
				]);
			}
			args.push('+delete');
			im.convert(args, function(err, stdout, stderr) {
				if (err) {
					cb(err);
					return;
				}
				var r = {};
				r[key] = pictures;
				cb(err, r);
			});
		});
	};
	var _createFileNames = function(filename, prefix, suffix, dimensions) {
		var pictures = {};
		for (var row in dimensions) {
			pictures[row] = prefix + filename + "_" + dimensions[row].width + 'x' + dimensions[row].height + suffix;
		}
		pictures.orig = prefix + filename + "_original" + suffix
		return pictures;
	};
	return {
		init: function(credentials) {
			_s3 = s3(credentials);
		},
		makeProfilePictures: function(path, userId, cb) {
			var date = Math.round(+new Date() / 1000);
			var parts = path.split('/');
			var filename = parts.pop().split('.');
			var destinationFolder = parts.join('/');
			var suffix = '_' + userId + '_' + date + '.' + filename[1];
			var pictures = _createFileNames(filename[0], "profile_", suffix, _profilePictureDimensions);
			_cropImages("images", path, destinationFolder, pictures, _profilePictureDimensions, 
				function(err, r) {
					if (err) {
						cb(err);
						return;
					}

					console.log(r);

					// self.transferToS3( r[key].standard, function() {
					// 	console.log(arguments);
						cb(err, r);
					// });
				}
			);
		},
		makeCover: function(path, userId, cb) {
			var date = Math.round(+new Date() / 1000);
			var parts = path.split('/');
			var filename = parts.pop().split('.');
			var destinationFolder = parts.join('/');
			var suffix = '_' + userId + '_' + date + '.' + filename[1];
			var pictures = _createFileNames(filename[0], "cover_", suffix, _profilePictureDimensions);
			_cropImages("images", path, destinationFolder, pictures, _coverDimensions, 
				function(err, r) {
					if (err) {
						console.error(err);
						return;
					}

					console.log(r);

					// self.transferToS3( r[key].standard, function() {
					// 	console.log(arguments);
						cb(err, r);
					// });
				}
			);
		},
		makePostImages: function(key, path, userId, cb) {
			var self = this;
			var date = Math.round(+new Date() / 1000);
			var parts = path.split('/');
			var filename = parts.pop().split('.');
			var destinationFolder = parts.join('/');
			var suffix = '_' + userId + '_' + date + '.' + filename[1];
			var pictures = _createFileNames(filename[0], "post_", suffix, _postDimensions);
			_cropImages(key, path, destinationFolder, pictures, _postDimensions,
				function(err, r) {
					if (err) {
						console.error(err);
						return;
					}

					console.log(r);

					// self.transferToS3( r[key].standard, function() {
					// 	console.log(arguments);
						cb(err, r);
					// });
				}
			);
		},
		transferToS3: function(file, cb) {
			_s3.put(file, fs.readFileSync('tmp/' + file), cb);
		}
	};
})();