var helper = require('../../../helper');
var util = require('../../../util');

module.exports = (function() {
	var _self;
	var _router = {};
	return {
		init: function(router) {
			_router = router;
			_self = this;
			return this;
		},
		getUserFollowingById: function(req, res) {
			_router.models.user_relation.getUserFollowingByUserId(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getUserFollowersById: function(req, res) {
			_router.models.user_relation.getUserFollowersByUserId(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getCurrentUserFollowingById: function(req, res) {
			req.params.id = req.session.user.id;
			_self.getUserFollowingById(req, res);
		},
		getCurrentUserFollowersById: function(req, res) {
			req.params.id = req.session.user.id;
			_self.getUserFollowersById(req, res);
		},
		followUserById: function(req, res) {
			var userId = req.session.user.id;
			var friendId = req.body.friendId;
			// check, friend exist
			_router.models.user.getUser(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}

				_router.models.user_relation.getRelationIdByUserAndFriendId(userId, friendId, function(err, result) {
					if (err) {
						helper.throwApiError(res, 103, 'Something went wrong.');
						return;
					}
					console.log(result);
					helper.sendData(res, result);
				});

			});

		},
		unfollowUserById: function(req, res) {
			var userId = req.session.user.id;
			var friendId = req.params.id;
		}
	};
});