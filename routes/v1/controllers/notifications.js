var helper = require('../../../helper');
var util = require('../../../util');
var notifAgent = require('../../../notif-agent');

module.exports = (function() {
	var _self;
	var _router = {};
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		getUserNotifications: function(req, res) {
			var userId = req.session.user.id;
			_router.models.notification.getUserNotificationsByUserId(userId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 130, 'Post not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},

		// to do: validate

		sendNotification: function(req, res) {
			var fromId = req.session.user.id;
			var toId = req.params.user_id;
			var type = req.body.type;
			var object = req.body.object;

			_router.models.user.getUserProfileView(toId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				_router.models.notification.addUserNotificationFromUser(result.id, fromId, type, object, function(err, result) {
					if (err) {
						helper.throwApiError(res, 125, 'Unable to send notification.');
						return;
					}
					helper.sendData(res, result);
				});
			});
		},
		confirmNotification: function(req, res) {
			var userId = req.session.user.id;
			var notifId = req.body.notif_id;

			console.log(req.body);

			_router.models.user.getUserPublicView(userId, function(err, user) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}

				_router.models.notification.confirmNotification(notifId, user, function(err, notif) {
					if (err) {
						helper.throwApiError(res, 125, 'Unable to send notification.');
						return;
					}

					if(notif.type == "follow_request") {
						// to do: reláció
						_router.models.user_relation.getRelationIdByUserAndFriendId(notif.from_id, userId, function(err, result) {
							if (err) {
								helper.throwApiError(res, 103, 'Something went wrong.');
								return;
							}

							if(result) {
								_router.models.user_relation.restoreRelation(result.id, function(err, result) {
									if (err) {
										helper.throwApiError(res, 103, 'Something went wrong.');
										return;
									}
									
									notifAgent.sendNotificationToUser(notif.from_id,'follow_request_confirmation', user);

									helper.sendData(res, notif);
								});
							} else {
								_router.models.user_relation.addRelation(notif.from_id, userId, function(err, result) {
									if (err) {
										helper.throwApiError(res, 103, 'Something went wrong.');
										return;
									}

									notifAgent.sendNotificationToUser(notif.from_id,'follow_request_confirmation', user);

									helper.sendData(res, notif);
								});
							}
						});
					}
				});
			});
		}
	};
})();