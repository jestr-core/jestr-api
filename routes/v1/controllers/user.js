var helper = require('../../../helper');
var util = require('../../../util');

module.exports = (function() {
	var _self;
	var _router = {};
	return {
		init: function(router) {
			_router = router;
			_self = this;
			return this;
		},
		getUserById: function(req, res) {
			_router.models.user.getUserView(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getMe: function(req, res) {
			/* to do: getUserSession */
			// var userId = 14;

			// console.log(req.session);

			// _router.models.user.getUser(userId, function(err, result) {
			// if (err) {
			// helper.throwApiError(res, 120, 'User not found.');
			// return;
			// }
			// helper.sendData(res, result);
			// });

			helper.sendData(res, req.session.user);
		},
		updateProfile: function(req, res) {
			/* to do:
				- getUserSession #done
				- profile picture handling #done
			*/
			//var userId = 14;
			var userId = req.session.user.id;

			_router.models.user.updateProfile(userId, req.body, function(err, result) {
				console.log(arguments);
				console.log(req.body);
				console.log(err);
				if (err) {
					helper.throwApiError(res, 122, 'Profile update failure.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		updateProfilePicture: function(req, res) {
			/* to do: getUserSession */

			//var userId = 14;
			var userId = req.session.user.id;

			if (!req.files || !req.files.picture) {
				helper.throwApiError(res, 123, 'Profile picture update failure.');
				return;
			}

			util.makeProfilePictures(req.files.picture.path, 14, function(err, pictures) {
				if (err) {
					helper.throwApiError(res, 123, 'Profile picture update failure.');
					return;
				}

				console.log(pictures);

				_router.models.user.updateProfile(userId, {
					profile: {
						picture: pictures
					}
				}, function(err, result) {
					if (err) {
						helper.throwApiError(res, 122, 'Profile update failure.');
						return;
					}
					helper.sendData(res, result);
				});
			});
		}
	};
})();