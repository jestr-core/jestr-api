var helper = require('../../../helper');
var util = require('../../../util');

module.exports = (function(){
	var _router = {};
	return {
		init: function(router) {
			_router = router;
			return this;
		},
		getPostById: function(req, res) {
			_router.models.post.getPostView(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();