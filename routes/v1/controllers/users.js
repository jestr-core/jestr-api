var helper = require('../../../helper');
var util = require('../../../util');

module.exports = (function() {
	var _self;
	var _router = {};
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		getUserById: function(req, res) {
			_router.models.user.getUserProfileView(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getMe: function(req, res) {
			_router.models.user.getUser(req.session.user.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				delete result.password;
				helper.sendData(res, result);
			});
		},
		updateProfile: function(req, res) {
			/* to do:
				- getUserSession #done
				- profile picture handling #done
			*/
			//var userId = 14;
			var userId = req.session.user.id;

			_router.models.user.updateProfile(userId, req.body, function(err, result) {
				// console.log(arguments);
				// console.log(req.body);
				// console.log(err);
				if (err) {
					helper.throwApiError(res, 122, 'Profile update failure.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		updateProfilePicture: function(req, res) {
			/* to do: getUserSession */

			//var userId = 14;
			var userId = req.session.user.id;

			if (!req.files && (!req.files.picture || !req.files.cover)) {
				helper.throwApiError(res, 123, 'Profile picture update failure.');
				return;
			}

			for (var f in req.files) {
				if (req.files[f].type != "image/png" && req.files[f].type != "image/jpeg") {
					helper.throwApiError(res, 124, 'User picture upload failure.');
					return;
				}
			}

			if(req.files.picture) {
				util.makeProfilePictures(req.files.picture.path, userId, function(err, pictures) {
					if (err) {
						helper.throwApiError(res, 123, 'Profile picture update failure.');
						return;
					}
					_router.models.user.updateProfile(userId, {
						profile: {
							picture: pictures.images
						}
					}, function(err, result) {
						if (err) {
							helper.throwApiError(res, 122, 'Profile update failure.');
							return;
						}
						helper.sendData(res, result);
					});
				});
			} else if(req.files.cover) {
				util.makeCover(req.files.cover.path, userId, function(err, cover) {
					if (err) {
						helper.throwApiError(res, 123, 'Profile picture update failure.');
						return;
					}
					_router.models.user.updateProfile(userId, {
						profile: {
							cover: cover.images
						}
					}, function(err, result) {
						if (err) {
							helper.throwApiError(res, 122, 'Profile update failure.');
							return;
						}
						helper.sendData(res, result);
					});
				});
			}
		},
		deactivate: function(req, res) {
			var userId = req.session.user.id;
			_router.models.user.deactivateUser(userId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 124, 'Profile deactivation failure.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		searchUser: function(req, res) {
			var keyword = req.body.keyword;
			_router.models.user.searchUserByKeyword(keyword, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();