var helper = require('../../../helper');
var config = require('../../../config');

module.exports = (function() {
	var _router = {};
	return {
		init: function() {
			_router = module.parent.exports;
			return this;
		},
		getUserFeed: function(req, res) {
			var userId = req.session.user.id;
			var page = req.query.page || 0;
			_router.models.post.getFeedByUserId(userId, page, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getTrendingFeed: function(req, res) {
			var page = req.query.page || 0;
			_router.models.post.getTrendingPosts(page, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getHashtagCount: function(req, res) {
			var hash = req.params.hash;
			_router.models.hash.getHashtagCount(hash, function(err, result) {
				if (err) {
					helper.throwApiError(res, 140, 'Hashtag not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		searchPostsByHash: function(req, res) {
			var page = req.body.page || 0;
			var hash = req.params.hash;
			_router.models.post.searchPostsByHash(hash, page, function(err, result) {
				if (err) {
					helper.throwApiError(res, 140, 'Hashtag not found.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();