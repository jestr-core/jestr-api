var helper = require('../../../helper');
var notifAgent = require('../../../notif-agent');

module.exports = (function() {
	var _self;
	var _router = {};
	var _commentRegExp = /(#[a-z0-9][a-z0-9\-_]*)/ig;
	var _userNameRegExp = /^(?!.*\bRT\b)(?:.+\s)?@\w+/ig;
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		getCommentsByPostId: function(req, res) {
			var postId = req.params.id;
			var page = req.query.page || 0;
			_router.models.comment.getCommentsByPostId(postId, page, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		commentPostByPostId: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			var comment = req.body.message;
			_router.models.comment.commentOnPost(userId, postId, comment, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}

				var hashtags = comment.match(_commentRegExp);

				for (var hash in hashtags) {
					_router.models.hash.addHashtagToPostByPostId(result[0].post_id, hashtags[hash], helper.logArgs);
				}

				helper.sendData(res, result);
				_router.models.post.getPostPublicView(postId, function(err, result) {
					if (!err) {
						var mentions = comment.match(_userNameRegExp);

						console.log(mentions);

						var sendMention = function(err, mentioned) {
							if (!err) {
								_router.models.notification.addUserNotificationFromUser(mentioned.id, req.session.user.id, 'mentioned_you', result);
								notifAgent.sendNotificationToUser(mentioned.id, 'mentioned_you', req.session.user);
							}
						};

						for (var mention in mentions) {
							_router.models.user.getUserByUserName(mentions[mention], sendMention);
						}
						if(userId != result.author.id) {
							_router.models.notification.addUserNotificationFromUser(result.author.id, req.session.user.id, 'user_commented_on_post', result);
							notifAgent.sendNotificationToUser(result.author.id, 'user_commented_on_post', req.session.user);
						}
					}
				});
			});
		},
		getCoolersByPostId: function(req, res) {
			var postId = req.params.id;
			var page = req.query.page || 0;
			_router.models.cool.getCoolersByPostId(postId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		coolOnPostByPostId: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			_router.models.cool.getCoolByUserIdAndPostId(userId, postId, function(err, result) {

				if (!err) {
					_router.models.cool.restoreCoolOnPost(userId, postId, function(err, result) {
						if (err) {
							helper.throwApiError(res, 104, 'Something went wrong.');
							return;
						}

						console.log(result);

						helper.sendData(res, result);

						_router.models.post.getPostPublicView(postId, function(err, result) {
							if (!err && result.author.id != userId) {
								_router.models.notification.addUserNotificationFromUser(result.author.id, req.session.user.id, 'user_cooled_on_post', result);
								notifAgent.sendNotificationToUser(result.author.id, 'user_cooled_on_post', req.session.user);
							}
						});
					});
				} else {
					_router.models.cool.coolOnPost(userId, postId, function(err, result) {
						if (err) {
							helper.throwApiError(res, 105, 'Something went wrong.');
							return;
						}

						console.log(result);

						helper.sendData(res, result);

						_router.models.post.getPostPublicView(postId, function(err, result) {
							if (!err && result.author.id != userId) {
								_router.models.notification.addUserNotificationFromUser(result.author.id, req.session.user.id, 'user_cooled_on_post', result);
								notifAgent.sendNotificationToUser(result.author.id, 'user_cooled_on_post', req.session.user);
							}
						});
					});
				}
			});
		},
		deleteCoolByPostId: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			_router.models.cool.deleteCoolByUserIdAndPostId(userId, postId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		deleteCommentByCommentId: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			var commentId = req.params.comment_id;
			_router.models.comment.deleteCommentByUserIdAndPostIdAndCommenId(userId, postId, commentId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();