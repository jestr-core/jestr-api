/*
 * JestrAPI V1
 */

module.exports = (function() {
	var _versionPrefix = '/v1';
	var _controllers = ['auth', 'posts', 'users', 'interactions', 'relations', 'notifications', 'feed', 'messages'];
	var _models = ['comment', 'cool', 'hash', 'notification', 'post', 'user', 'user_relation'];
	var _routes = [
		// auth
		{
			path: '/auth/login',
			controller: 'auth',
			method: 'post',
			action: 'loginUser'
		}, {
			path: '/auth/register',
			controller: 'auth',
			method: 'post',
			action: 'registerUser'
		}, {
			path: '/auth/logout',
			controller: 'auth',
			method: 'delete',
			action: 'logoutUser'
		},
		{
			path: '/auth/token',
			controller: 'auth',
			method: 'post',
			action: 'regsiterAPNSToken'
		},
		// user
		{
			path: '/users/me',
			controller: 'users',
			method: 'get',
			action: 'getMe'
		}, {
			path: '/users/me',
			controller: 'users',
			method: 'put',
			action: 'updateProfile'
		}, {
			path: '/users/me',
			controller: 'users',
			method: 'post',
			action: 'updateProfilePicture'
		}, {
			path: '/users/me',
			controller: 'users',
			method: 'delete',
			action: 'deactivate'
		}, {
			path: '/users/me/following',
			controller: 'relations',
			method: 'get',
			action: 'getCurrentUserFollowingById'
		}, {
			path: '/users/me/followers',
			controller: 'relations',
			method: 'get',
			action: 'getCurrentUserFollowersById'
		}, {
			path: '/users/me/notifications',
			controller: 'notifications',
			method: 'get',
			action: 'getUserNotifications'
		}, {
			path: '/users/me/notifications',
			controller: 'notifications',
			method: 'put',
			action: 'confirmNotification'
		}, {
			path: '/users/me/posts',
			controller: 'posts',
			method: 'get',
			action: 'getCurrentUserPosts'
		}, {
			path: '/users/me/feed',
			controller: 'feed',
			method: 'get',
			action: 'getUserFeed'
		},
		// everybody else
		{
			path: '/users/:id(\\d+)',
			controller: 'users',
			method: 'get',
			action: 'getUserById'
		}, {
			path: '/users/:id(\\d+)/following',
			controller: 'relations',
			method: 'get',
			action: 'getUserFollowingById'
		}, {
			path: '/users/:id(\\d+)/followers',
			controller: 'relations',
			method: 'get',
			action: 'getUserFollowersById'
		}, {
			path: '/users/:id(\\d+)/followers',
			controller: 'relations',
			method: 'post',
			action: 'followUserById'
		}, {
			path: '/users/:id(\\d+)/followers',
			controller: 'relations',
			method: 'delete',
			action: 'unfollowUserById'
		}, {
			path: '/users/:id(\\d+)/notifications',
			controller: 'notifications',
			method: 'post',
			action: 'sendNotification'
		}, {
			path: '/users/:id(\\d+)/posts',
			controller: 'posts',
			method: 'get',
			action: 'getUserPostsByUserId'
		}, {
			path: '/users/search',
			controller: 'users',
			method: 'post',
			action: 'searchUser'
		},
		// {
		// 	path: '/users/:id(\\d+)/messages',
		// 	controller: 'posts',
		// 	method: 'get',
		// 	action: 'getUserMessagesByUserId'
		// },
		// post
		{
			path: '/posts/:id(\\d+)',
			controller: 'posts',
			method: 'get',
			action: 'getPostById'
		}, {
			path: '/posts/:id(\\d+)',
			controller: 'posts',
			method: 'delete',
			action: 'deletePostByPostId'
		}, {
			path: '/posts/:id(\\d+)/comments',
			controller: 'interactions',
			method: 'post',
			action: 'commentPostByPostId'
		}, {
			path: '/posts/:id(\\d+)/comments',
			controller: 'interactions',
			method: 'get',
			action: 'getCommentsByPostId'
		}, {
			path: '/posts/:id(\\d+)/comments/:comment_id(\\d+)',
			controller: 'interactions',
			method: 'delete',
			action: 'deleteCommentByCommentId'
		}, {
			path: '/posts/:id(\\d+)/coolers',
			controller: 'interactions',
			method: 'get',
			action: 'getCoolersByPostId'
		}, {
			path: '/posts/:id(\\d+)/coolers',
			controller: 'interactions',
			method: 'post',
			action: 'coolOnPostByPostId'
		}, {
			path: '/posts/:id(\\d+)/coolers',
			controller: 'interactions',
			method: 'delete',
			action: 'deleteCoolByPostId'
		}, {
			path: '/posts/trending',
			controller: 'feed',
			method: 'get',
			action: 'getTrendingFeed'
		},
		// hashtags
		{
			path: '/tags/:hash',
			controller: 'feed',
			method: 'get',
			action: 'getHashtagCount'
		}, {
			path: '/tags/:hash/posts',
			controller: 'feed',
			method: 'get',
			action: 'searchPostsByHash'
		},
		// publish
		{
			path: '/users/me/posts',
			controller: 'posts',
			method: 'post',
			action: 'publishPost'
		},
		// messages
		{
			path: '/users/me/messages',
			controller: 'messages',
			method: 'get',
			action: 'getUserMessageThreads'
		},
		{
			path: '/users/me/messages',
			controller: 'messages',
			method: 'post',
			action: 'sendMessages'
		},
		{
			path: '/users/me/messages/:friend_id(\\d+)',
			controller: 'messages',
			method: 'get',
			action: 'getMessageThreadByFriendId'
		},
		{
			path: '/users/me/messages/:friend_id(\\d+)',
			controller: 'messages',
			method: 'post',
			action: 'sendMessageToFriend'
		},
	];
	return {
		models: [],
		controllers: [],
		init: function() {
			/* Loading models */
			for (var model in _models) {
				this.models[_models[model]] = require('./models/' + _models[model]);
			}

			/* Loading controllers */

			for (var controller in _controllers) {
				this.controllers[_controllers[controller]] = require('./controllers/' + _controllers[controller])
					.init();
			}

			this.__defineGetter__('routes', function() {
				return _routes;
			});

			this.__defineGetter__('prefix', function() {
				return _versionPrefix;
			});

			return this;
		}
	};
})();