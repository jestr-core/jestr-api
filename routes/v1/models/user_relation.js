var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"user_id": {
			dataType: "numeric"
		},
		"friend_id": {
			dataType: "numeric"
		}
	};
	var _table = 'user_relation';
	var _views = ['user_relation_following_view', 'user_relation_followers_view'];
	var _queries = {
		following: "SELECT array_to_json(array_agg(\"row\".\"friend\")) AS data FROM ( SELECT \"friend\" FROM \"" + _views[0] + "\" WHERE \"user_id\" = '{user_id}') \"row\";",
		followers: "SELECT array_to_json(array_agg(\"row\".\"follower\")) AS data FROM ( SELECT \"follower\" FROM \"" + _views[1] + "\" WHERE \"user_id\" = '{user_id}') \"row\";"
	};
	return {
		getUserFollowingByUserId: function(userId, cb) {
			db.query(_queries.following, {
				user_id: userId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0].data);
			});
		},
		getUserFollowersByUserId: function(userId, cb) {
			db.query(_queries.followers, {
				user_id: userId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0].data);
			});
		},
		getRelationIdByUserAndFriendId: function(userId, friendId, cb) {
			db.getRow(["id"], _table, {
				user_id: userId,
				friend_id: friendId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		addRelation: function(userId, friendId, cb) {
			db.add(_table, {
				user_id: userId,
				friend_id: friendId,
				date: Math.round(+new Date() / 1000)
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0]);
			});

		},
		restoreRelation: function(relationId, cb) {
			db.set(_table, {
				deleted: false
			}, {
				id: relationId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0]);
			});
		},
		deleteRelation: function(userId, friendId, cb) {
			this.getRelationIdByUserAndFriendId(userId, friendId, function(err, result) {
				console.log(arguments);
				db.set(_table, {
					deleted: true
				}, {
					id: result.id,
				}, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					cb(err, result[0]);
				});
			});
		}
	};
})();