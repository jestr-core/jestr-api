var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"user_id": {
			dataType: "numeric"
		},
		"from_id": {
			dataType: "numeric"
		},
		"type": {
			dataType: "enum:mentioned_you|started_following_you|follow_request|follow_request_confirmation|new_private_message|user_commented_on_post|user_cooled_on_post|friend_joined"
		},
		"related_object": {
			dataType: "object",
			strictObject: false
		}
	};
	var _table = 'notification';
	var _views = ['notification_public_view'];
	return {
		getNotificationById: function(notifId, cb) {
			db.getRow(["*"], _table, {
				id: notifId,
				deleted: false
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getUserNotificationsByUserId: function(userId, cb) {
			db.get(["id", "type", "from", "confirmed", "related_object", "created_time"], _views[0], {
				user_id: userId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		addUserNotificationFromUser: function(userId, fromId, type, object, cb) {
			// to do: separate handlers
			var fields = {
				user_id: userId,
				from_id: fromId,
				type: type,
				related_object: object
			};

			if (helper.validate(fields, _attributes)) {
				fields.date = Math.round(+new Date() / 1000);
				db.add(_table, fields, cb);
				return;
			}
			cb('Validation error');
		},
		confirmNotification: function(notifId, user, cb) {
			// follow_request|follow_request_confirmation|new_private_message|user_commented_on_post|user_cooled_on_post|friend_joined
			var self = this;
			self.getNotificationById(notifId, function(err, result) {
				if (err) {
					cb(err);
					return;
				}

				var notif = result;

				switch (notif.type) {
					case "friend_joined":
					case "new_private_message":
					case "user_commented_on_post":
					case "user_cooled_on_post":
					case "follow_request_confirmation":
						db.set(_table, {
							confirmed: true
						}, {
							id: notifId
						}, function(err, result) {
							if (err) {
								cb(err);
								return;
							}
							notif.confirmed = true;
							cb(err, notif);
						});
						break;
					case "follow_request":
						self.addUserNotificationFromUser(
							notif.from_id,
							notif.user_id,
							'follow_request_confirmation',
							user,
							function(err, result) {
							if (err) {
								cb(err);
								return;
							}

							db.set(_table, {
								confirmed: true
							}, {
								id: notifId
							}, function(err, result) {
								if (err) {
									cb(err);
									return;
								}
								notif.confirmed = true;
								cb(err, notif);
							});

						});
						break;
				}
			});
		}
	};
})();