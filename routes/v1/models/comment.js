var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _limit = 100;
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"user_id": {
			dataType: "numeric"
		},
		"post_id": {
			dataType: "numeric"
		},
		"message": {
			dataType: "string"
		}
	};
	var _table = 'comment';
	var _views = ['comment_public_view'];
	return {
		commentOnPost: function(userId, postId, message, cb) {
			if (helper.validate({
				user_id: userId,
				post_id: postId,
				message: message
			}, _attributes)) {
				db.add(_table, {
					user_id: userId,
					post_id: postId,
					message: message,
					date: Math.round(+new Date() / 1000)
				}, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					cb(err, result);
				});
				return;
			}
			cb('Validation error');
		},
		getCommentsByPostId: function(postId, page, cb) {
			db.get(["id","from","message","created_time"], _views[0], {
				post_id: postId
			}, {
				page: page,
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		deleteCommentByUserIdAndPostIdAndCommenId: function(userId, postId, commentId, cb){
			db.set(_table, {
				deleted: true
			}, {
				id: commentId,
				user_id: userId,
				post_id: postId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		}
	};
})();