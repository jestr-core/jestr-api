var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"username": {
			dataType: "username",
			notNull: true
		},
		"password": {
			dataType: "string",
		},
		"profile": {
			dataType: "object",
			strictObject: false,
			"about": {
				dataType: "string"
			},
			"gender": {
				dataType: "enum:male|female|unset"
			},
			"picture": {
				dataType: "object",
				"small": {
					dataType: "string"
				},
				"medium": {
					dataType: "string"
				},
				"large": {
					dataType: "string"
				},
				"xlarge": {
					dataType: "string"
				}
				// ,
				// "xxlarge": {
				// 	dataType: "string"
				// }
			},
			"cover": {
				dataType: "object",
				"small": {
					dataType: "string"
				},
				"medium": {
					dataType: "string"
				},
				"large": {
					dataType: "string"
				},
				"xlarge": {
					dataType: "string"
				}
			}
		},
		"email": {
			dataType: "email",
			notNull: true
		},
		"location": {
			dataType: "boolean"
		},
		"private": {
			dataType: "boolean"
		}
	};
	var _table = 'user';
	var _views = ['user_public_view', 'user_profile_view_prefetched'];
	var _queries = {
		//search: 'SELECT * from "' + _views[1] + '" WHERE lower(username) LIKE \'{keyword}%\' OR lower(profile->>\'about\') LIKE \'%{keyword}%\''
		search: 'SELECT * from "' + _views[0] + '" WHERE lower(username) LIKE \'{keyword}%\''
	};
	return {
		getUserPublicView: function(userId, cb) {
			db.getRow(["*"], _views[0], {
				id: userId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getUserProfileView: function(userId, cb) {
			db.getRow(["*"], _views[1], {
				id: userId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getUser: function(userId, cb) {
			db.getRow(["*"], _table, {
				id: userId,
				deleted: false
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getUserByUserName: function(username, cb) {
			if(username.charAt(0) == "@") {
				username = username.substr(1, username.length);
			}
			db.getRow(["*"], _table, {
				username: username,
				deleted: false
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		loginUser: function(username, password, cb) {
			db.getRow(["id","username","email","location","private","profile","lastUpdate","lastLogin"], _table, {
				username: username,
				password: password,
				deleted: false
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}

				result.lastLogin = Math.round(new Date() / 1000);

				cb(err, result);
				db.set(_table, {
					"lastLogin": result.lastLogin,
					"deleted": false
				}, {
					id: result.id,
				});
			});
		},
		registerUser: function(username, password, email, cb) {
			var self = this;
			if (helper.validate({
				username: username,
				password: password,
				email: email
			}, _attributes)) {
				var currentDate = Math.round(+new Date() / 1000);
				db.add(_table, {
					username: username,
					password: password,
					email: email,
					regDate: currentDate,
					lastLogin: currentDate,
				}, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					self.getUser(result[0].id, cb);
				});
				return;
			}
			cb('Validation error');
		},
		updateProfile: function(userId, fields, cb) {
			var self = this;
			if (helper.validate(fields, _attributes)) {
				fields.lastUpdate = Math.round(+new Date() / 1000);
				db.set(_table, fields, {
					id: userId,
				}, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					self.getUserProfileView(result[0].id, cb);
				});
				return;
			}
			cb('Validation error');
		},
		deactivateUser: function(userId, cb) {
			db.set(_table, {
				deleted: true
			}, {
				id: userId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				self.getUserProfileView(result[0].id, cb);
			});
		},
		searchUserByKeyword: function(keyword, cb) {
			db.query(_queries.search, {
				keyword: keyword.toLowerCase()
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		}
	};
})();