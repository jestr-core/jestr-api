var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _limit = 20;
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"user_id": {
			dataType: "numeric"
		},
		"to_id": {
			dataType: "numeric"
		},
		"images": {
			dataType: "object",
			"layer": {
				dataType: "object",
				"small": {
				dataType: "string"
				},
				"medium": {
					dataType: "string"
				},
				"large": {
					dataType: "string"
				}
			},
			"background": {
				dataType: "object",
				"small": {
					dataType: "string"
				},
				"medium": {
					dataType: "string"
				},
				"large": {
					dataType: "string"
				}
			},
			"cropped": {
				dataType: "object",
				"small": {
					dataType: "string"
				},
				"medium": {
					dataType: "string"
				},
				"large": {
					dataType: "string"
				}
			}
		}
	};
	var _table = 'post';
	var _views = ['post_public_view', 'post_feed_view', 'post_explore_view', 'post_search_view', 'post_thread_preview', 'post_thread_view'];
	return {
		getPostPublicView: function(postId, cb) {
			// to do: check if user have rights to view
			db.getRow(["id", "author", "images", "comments", "coolerz", "hashtags", "created_time", "user_id", "to_id"], _views[0], {
				id: postId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getPostsByUserId: function(userId, lastId, cb) {
			var params = {
				user_id: userId,
				to_id: userId,
			};
			if(lastId) {
				params.id = {
					operand: '<',
					value: lastId
				};
			}
			db.get(["id", "author", "images", "comments", "coolerz", "hashtags", "created_time"], _views[0], params, {
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getFeedByUserId: function(userId, lastId, cb) {
			var params = {
				user_id: userId
			};
			if(lastId) {
				params.id = {
					operand: '<',
					value: lastId
				};
			}
			db.get(["id", "author", "images", "comments", "coolerz", "hashtags", "created_time"], _views[1], params, {
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getTrendingPosts: function(lastId, cb) {
			var params = {};
			if(lastId) {
				params.id = {
					operand: '<',
					value: lastId
				};
			}
			db.get(["id", "author", "images", "comments", "coolerz", "hashtags", "created_time"], _views[2], params, {
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		searchPostsByHash: function(hash, lastId, cb) {
			var params = {
				hash: hash
			};
			if(lastId) {
				params.id = {
					operand: '<',
					value: lastId
				};
			}
			db.get(["id", "author", "images", "comments", "coolerz", "hashtags", "created_time"], _views[3], params, {
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getPrivateMessageThreadListByUserId: function(userId, cb) {
			db.get(["friend", "posts"], _views[4], {
				user_id: userId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		getPrivateMessagesByUserIdAndFriendId: function(userId, friendId, lastId, cb) {
			var params = {
				user_id: userId,
				to_id: friendId
			};
			if(lastId) {
				params.id = {
					operand: '<',
					value: lastId
				};
			}
			db.get(["id", "author", "images", "created_time"], _views[5], params, {
				limit: _limit
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		addPost: function(fields, cb) {
			if (helper.validate(fields, _attributes)) {
				fields.date = Math.round(+new Date() / 1000);
				fields.trending = 10;
				db.add(_table, fields, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					cb(err, result);
				});
			}
		},
		deletePost: function(userId, postId, cb) {
			db.set(_table, {
				deleted: true
			}, {
				id: postId,
				user_id: userId,
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		}
	};
})();