var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"user_id": {
			dataType: "numeric"
		},
		"post_id": {
			dataType: "numeric"
		}
	};
	var _table = 'cool';
	var _views = ['cool_public_view'];
	var _queries = {
		coolers: "SELECT array_to_json(array_agg(\"row\".\"user\")) AS data FROM ( SELECT \"user\" FROM \"" + _views[0] + "\" WHERE \"post_id\" = '{post_id}') \"row\";",
	};
	return {
		coolOnPost: function(userId, postId, cb) {
			if (helper.validate({
				user_id: userId,
				post_id: postId
			}, _attributes)) {
				db.add(_table, {
					user_id: userId,
					post_id: postId,
					date: Math.round(+new Date() / 1000)
				}, function(err, result) {
					if (err) {
						cb(err);
						return;
					}
					cb(err, result);
				});
				return;
			}
			cb('Validation error');
		},
		getCoolersByPostId: function(postId, cb) {
			db.query(_queries.coolers, {
				post_id: postId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0].data);
			});
		},
		getCoolByUserIdAndPostId: function(userId, postId, cb) {
			db.getRow(["*"], _table, {
				user_id: userId,
				post_id: postId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		restoreCoolOnPost: function(userId, postId, cb){
			db.set(_table, {
				deleted: false
			}, {
				user_id: userId,
				post_id: postId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		deleteCoolByUserIdAndPostId: function(userId, postId, cb){
			db.set(_table, {
				deleted: true
			}, {
				user_id: userId,
				post_id: postId
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		}
	};
})();