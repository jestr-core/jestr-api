var db = require('../../../database');
var helper = require('../../../helper');

module.exports = (function() {
	var _attributes = {
		"id": {
			dataType: "numeric"
		},
		"hash": {
			dataType: "hashtag"
		},
		"post_id": {
			dataType: "numeric"
		}
	};
	var _table = 'hash';
	var _views = ['hash_public_view', 'hash_search_view'];
	return {
		getHashtagCount: function(hash, cb) {
			db.getRow(["*"], _views[1], {
				hash: hash
			}, function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result);
			});
		},
		addHashtagToPostByPostId: function(postId, hashtag, cb) {
			hashtag = hashtag.substr(1, hashtag.length);
			if (helper.validate({
				post_id: postId,
				hash: hashtag
			}, _attributes)) {
				db.add(_table, {
					post_id: postId,
					hash: hashtag
				});
				return;
			}
			cb('Validation error');
		}
	};
})();