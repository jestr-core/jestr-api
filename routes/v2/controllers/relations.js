var helper = require('../../../helper');
var util = require('../../../util');
var notifAgent = require('../../../notif-agent');

module.exports = (function() {
	var _self;
	var _router = {};
	var _checkFriendAndRelation = function(res, userId, friendId, cb) {
		// check, if friend is exist
		_router.models.user.getUserPublicView(friendId, function(err, user) {
			if (err || !user.id) {
				helper.throwApiError(res, 120, 'User not found.');
				return;
			}
			// check, if relation is exist

			_router.models.user_relation.getRelationIdByUserAndFriendId(userId, friendId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, 'Something went wrong.');
					return;
				}
				cb(result, user);
			});
		});
	};
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		getUserFollowingById: function(req, res) {
			_router.models.user_relation.getUserFollowingByUserId(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getUserFollowersById: function(req, res) {
			_router.models.user_relation.getUserFollowersByUserId(req.params.id, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getCurrentUserFollowingById: function(req, res) {
			req.params.id = req.session.user.id;
			_self.getUserFollowingById(req, res);
		},
		getCurrentUserFollowersById: function(req, res) {
			req.params.id = req.session.user.id;
			_self.getUserFollowersById(req, res);
		},
		followUserById: function(req, res) {
			var userId = req.session.user.id;
			var friendId = req.params.id;
			_checkFriendAndRelation(res, userId, friendId, function(result, user) {
				if (result.id) {
					_router.models.user.getUserProfileView(friendId, function(err, profile) {
						if (err) {
							helper.throwApiError(res, 103, 'Something went wrong.');
							return;
						}

						_router.models.user.getUserPublicView(userId, function(err, pu) {
							if (err) {
								helper.throwApiError(res, 103, 'Something went wrong.');
								return;
							}


							if(!profile.private) {
								_router.models.notification.addUserNotificationFromUser(friendId, userId, 'started_following_you', pu);
								notifAgent.sendNotificationToUser(friendId, 'started_following_you', pu);
								_router.models.user_relation.restoreRelation(result.id, function(err, result) {
									if (err) {
										helper.throwApiError(res, 103, 'Something went wrong.');
										return;
									}
									helper.sendData(res, pu);
								});
							} else {
								_router.models.notification.addUserNotificationFromUser(friendId, userId, 'follow_request', pu);
								notifAgent.sendNotificationToUser(friendId, 'follow_request', pu);
								helper.sendData(res, {});
							}
						});
					});
				} else {
					_router.models.user.getUserProfileView(friendId, function(err, profile) {
						if (err) {
							helper.throwApiError(res, 103, 'Something went wrong.');
							return;
						}

						_router.models.user.getUserPublicView(userId, function(err, pu) {
							if (err) {
								helper.throwApiError(res, 103, 'Something went wrong.');
								return;
							}

							if(!profile.private) {
								_router.models.notification.addUserNotificationFromUser(friendId, userId, 'started_following_you', pu);
								notifAgent.sendNotificationToUser(friendId, 'started_following_you', pu);
								_router.models.user_relation.addRelation(userId, friendId, function(err, result) {
									if (err) {
										helper.throwApiError(res, 103, 'Something went wrong.');
										return;
									}
									helper.sendData(res, pu);
								});
							} else {
								_router.models.notification.addUserNotificationFromUser(friendId, userId, 'follow_request', pu);
								notifAgent.sendNotificationToUser(friendId, 'follow_request', pu);
								helper.sendData(res, {});
							}
						});
					});
				}
			});
		},
		unfollowUserById: function(req, res) {
			var userId = req.session.user.id;
			var friendId = req.params.id;
			_checkFriendAndRelation(res, userId, friendId, function(result) {
				console.log(arguments);

				if (result.id) {
					_router.models.user_relation.deleteRelation(userId, friendId, function(err, result) {
						console.log(arguments);
						if (err) {
							helper.throwApiError(res, 108, 'Something went wrong.');
							return;
						}
						helper.sendData(res, result);
					});
					return;
				} else {
					helper.throwApiError(res, 109, 'Something went wrong.');
				}
			});
		}
	};
})();