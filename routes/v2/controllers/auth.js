var helper = require('../../../helper');
var config = require('../../../config');
var crypto = require('crypto');
var redis = require('../../../redis');

module.exports = (function() {
	var _router = {};
	return {
		init: function() {
			_router = module.parent.exports;
			return this;
		},
		loginUser: function(req, res) {
			var s = crypto.createHash('sha1');
			s.update(req.body.password + config.crypto.passwordSalt);
			var password = s.digest('hex');
			_router.models.user.loginUser(req.body.username, password, function(err, result) {
				if (err) {
					helper.throwApiError(res, 120, 'User not found.');
					return;
				}
				//req.session.cookie.maxAge = new Date(Date.now() + (1000 * 60 * 60 * 24 * 3));
				req.session.user = {
					id: result.id,
					username: result.username,
					online: true
				};
				req.session.save(function() {
					req.session.touch();
					console.log(req.session);
					console.log(arguments);
					helper.sendData(res, result);
				});
			});
		},
		registerUser: function(req, res) {
			var s = crypto.createHash('sha1');
			s.update(req.body.password + config.crypto.passwordSalt);
			var password = s.digest('hex');
			_router.models.user.registerUser(req.body.username.trim(), password, req.body.email, function(err, result) {
				if (err) {
					console.log(err);
					helper.throwApiError(res, 121, 'Registration failure.');
					return;
				}

				_router.models.user_relation.addRelation(result.id, result.id, helper.logArgs);
				//req.session.touch();
				//req.session.cookie.maxAge = new Date(Date.now() + (1000 * 60 * 60 * 24 * 3));
				req.session.user = {
					id: result.id,
					username: result.username,
					online: true
				};
				req.session.save(function() {
					req.session.touch();
					helper.sendData(res, result);
				});
			});
		},
		logoutUser: function(req, res) {
			req.session.destroy(function() {
				helper.sendData(res, {
					good: 'bye'
				});
			});
		},
		regsiterAPNSToken: function(req, res) {
			console.log(req.session.user);
			redis.setToken(req.session.user.id, helper.formatToken(req.body.token));
			console.log("User [" + req.session.user.id + "] registered with: " + req.body.token);
			
			redis.getToken(req.session.user.id, function(err, result) {
				if (err) {
					console.log(err);
					helper.throwApiError(res, 126, 'Token not found.');
					return;
				}
				console.log("Result: " + result);
				helper.sendData(res, result);
			});
		}
		// userIdle
		// userWakeup
	};
})();