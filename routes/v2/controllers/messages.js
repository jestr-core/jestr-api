var helper = require('../../../helper');
var utility = require('../../../util');
var notifAgent = require('../../../notif-agent');

module.exports = (function() {
	var _self = {};
	var _router = {};
	var _makeImages = function(paths, userId, cb) {
		var result = {};
		var cnt = 0;

		var keys = Object.keys(paths);

		var resultCheck = function(err, pictures) {

			console.log("done " + cnt);

			if (err) {
				helper.throwApiError(res, 133, 'Post picture upload failure.');
				return;
			}
			helper.mergeJson(result, pictures);
			cnt++;
			if (cnt === 3) {
				cb(result);
			} else {
				utility.makePostImages(keys[cnt], paths[keys[cnt]], userId, resultCheck);
			}
		};
		utility.makePostImages(keys[0], paths[keys[0]], userId, resultCheck);
	};
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		sendMessages: function(req, res) {
			var userId = req.session.user.id;
			if (!req.files || !req.files.background || !req.files.layer || !req.files.cropped) {
				helper.throwApiError(res, 131, 'Post picture upload failure.');
				return;
			}

			var toIds = req.body.recipents;

			for (var f in req.files) {
				if (req.files[f].type != "image/png" && req.files[f].type != "image/jpeg") {
					helper.throwApiError(res, 132, 'Post picture upload failure.');
					return;
				}
			}

			_makeImages({
				background: req.files.background.path,
				layer: req.files.layer.path,
				cropped: req.files.cropped.path
			}, userId, function(pictures) {
				var results = [],
					resultIds = [];
				var postCount = toIds.length;
				var onComplete = function(err, result) {
												console.log(notifAgent);

					if (err) {
						helper.throwApiError(res, 103, ' Something went wrong.');
						return;
					}
					results.push(result[0]);
					resultIds.push(result[0].id);
					if (postCount--) {
						helper.sendData(res, resultIds);
						for (var row in results) {
							_router.models.notification.addUserNotificationFromUser(results[row].to_id, results[row].user_id, 'new_private_message', results[row]);
							notifAgent.sendNotificationToUser(results[row].to_id, 'new_private_message', req.session.user);
						}
					}
				};
				for (var toId in toIds) {
					_router.models.post.addPost({
						user_id: userId,
						to_id: toIds[toId],
						images: pictures
					}, onComplete);
				}
			});
		},
		sendMessageToFriend: function(req, res) {
			req.body.recipents = [req.params.friend_id];
			_self.sendMessages(req, res);
		},
		getUserMessageThreads: function(req, res) {
			var userId = req.session.user.id;
			_router.models.post.getPrivateMessageThreadListByUserId(userId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, ' Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getMessageThreadByFriendId: function(req, res) {
			var userId = req.session.user.id;
			var friendId = req.params.friend_id;
			var lastId = req.body.lastId || 0;
			_router.models.post.getPrivateMessagesByUserIdAndFriendId(userId, friendId, lastId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, ' Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();