var helper = require('../../../helper');
var utility = require('../../../util');

module.exports = (function() {
	var _self = {};
	var _router = {};
	var _makeImages = function(paths, userId, cb) {
		var result = {};
		var cnt = 0;

		var keys = Object.keys(paths);

		var resultCheck = function(err, pictures) {

			console.log("done " + cnt);

			if (err) {
				helper.throwApiError(res, 133, 'Post picture upload failure.');
				return;
			}
			helper.mergeJson(result, pictures);
			cnt++;
			if (cnt === 3) {
				cb(result);
			} else {
				utility.makePostImages(keys[cnt], paths[keys[cnt]], userId, resultCheck);
			}
		};
		utility.makePostImages(keys[0], paths[keys[0]], userId, resultCheck);
	};
	return {
		init: function() {
			_router = module.parent.exports;
			_self = this;
			return this;
		},
		getPostById: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			_router.models.post.getPostPublicView(postId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 130, 'Post not found.');
					return;
				}
				// user_id equals userId // own
				// to_id equals userId // message
				// to_id equals user id // public
				if (
					result.user_id == userId ||
					result.user_id == result.to_id ||
					result.to_id == userId
				) {
					helper.sendData(res, result);
					return;
				}
				helper.throwApiError(res, 130, 'Post not found.');
			});
		},
		getUserPostsByUserId: function(req, res) {
			var lastId = req.query.lastId || false;
			var userId = req.params.id;

			_router.models.post.getPostsByUserId(userId, lastId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, ' Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		getCurrentUserPosts: function(req, res) {
			req.params.id = req.session.user.id;
			_self.getUserPostsByUserId(req, res);
		},
		getUserMessagesByUserId: function(req, res) {
			var lastId = req.query.lastId || false;
			var userId = req.session.user.id;
			var friendId = req.params.id;
			_router.models.post.getPrivateMessagesByUserIdAndFriendId(userId, friendId, lastId, function(err, result) {
				if (err) {
					helper.throwApiError(res, 103, ' Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		},
		publishPost: function(req, res) {
			var userId = req.session.user.id;
			if (!req.files || !req.files.background || !req.files.layer || !req.files.cropped) {
				helper.throwApiError(res, 131, 'Post picture upload failure.');
				return;
			}

			for (var f in req.files) {
				if (req.files[f].type != "image/png" && req.files[f].type != "image/jpeg") {
					helper.throwApiError(res, 132, 'Post picture upload failure.');
					return;
				}
			}

			_makeImages({
				background: req.files.background.path,
				layer: req.files.layer.path,
				cropped: req.files.cropped.path
			}, userId, function(pictures) {
				_router.models.post.addPost({
					user_id: userId,
					to_id: userId,
					images: pictures
				}, function(err, result) {
					if (err) {
						helper.throwApiError(res, 103, ' Something went wrong.');
						return;
					}

					console.log(result);

					helper.sendData(res, [{
						id: result[0].id
					}]);
					
					var caption = req.body.caption;
					if(caption.trim().length > 0) {
						_router.models.comment.commentOnPost(userId, result[0].id, caption, helper.logArgs);
					}

					var hashtags = caption.match(/(\S*#\[[^\]]+\])|(\S*#\S+)/gi);

					for (var hash in hashtags) {
						_router.models.hash.addHashtagToPostByPostId(result[0].id, hashtags[hash], helper.logArgs);
					}
				});
			});
		},
		deletePostByPostId: function(req, res) {
			var userId = req.session.user.id;
			var postId = req.params.id;
			_router.models.post.deletePost(userId, postId, function(err, result) {
				console.log(arguments);
				if (err) {
					helper.throwApiError(res, 103, ' Something went wrong.');
					return;
				}
				helper.sendData(res, result);
			});
		}
	};
})();