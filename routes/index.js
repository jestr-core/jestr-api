/*
 * GET home page.
 */

module.exports = (function() {
	var _routers = [];
	return {
		setUpRoutes: function(versions) {
			for (var version in versions) {
				_routers[versions[version]] = require('./' + versions[version]).init();
			}
			this.__defineGetter__('routers', function(){
				return _routers;
			});
			return this;
		}
	};
})();