/**
 * @name JestrAPI
 * @author Árpád Kiss
 * @since 2014
 */

/**
 * New Relic
 */

require('newrelic');
//var agent = require('webkit-devtools-agent');

var PORT = process.env.NODE_PORT || 8080;

//console.log(process.env);

/**
 * Module dependencies.
 */

var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');

var http = require('http');
var path = require('path');
var helper = require('./helper');
var config = require('./config');
var database = require('./database')
    .init(helper.buildDatabaseConnectionString(config.postgres));

var redis = require('./redis').init(config.redis.port, config.redis.host);

var RedisStore = require('connect-redis')(session);

var notifAgent = require('./notif-agent').init(config.apns.key, config.apns.cert);

require('./util').init(config.s3);

var app = express();

app.use(require('compression')());

//app.use(express.favicon());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(require('connect-multiparty')({
    uploadDir: './tmp'
}));

app.use(require('cookie-parser')(config.crypto.cookieKey));
//app.use(express.session());
app.use(require('method-override')());
app.use(require('morgan')('dev'));

app.locals.maxAge = 1000 * 60 * 60 * 24 * 30;

app.use(session({
    key: 'jstr.sess',
    store: new RedisStore({
        // port: config.redis.port,
        // host: config.redis.host,
        // db: config.redis.database,
        // pass: config.redis.password,
        client: redis.client,
        prefix: 'jstr.sess'
    }),
    secret: config.crypto.cookieKey,
    proxy: true, // necessary if you're behind a proxy
    cookie: {
        //secure: true,
        //expires: new Date(Date.now() + app.locals.maxAge),
        maxAge: app.locals.maxAge
    }
}));

/* middlewares */

app.use(require('serve-static')(__dirname + '/tmp'));

app.locals.version = config.
default;
app.locals.versions = config.versions;
app.locals.endPoint = "";


app.use(function(req, res, next) {
    var path = req.path.split('/');
    var apiVersion = path[1];
    path.shift();
    path.shift();
    if (app.locals.versions.indexOf(apiVersion) === -1) {
        helper.throwApiError(res, 100, 'API Version is not available.');
        return;
    }
    app.locals.version = apiVersion;
    app.locals.endPoint = '/' + path.join('/');
    app.locals.prefix = path[0];
    next();
});

/* auth check */
app.use(function(req, res, next) {
    if (!req.session.user && app.locals.prefix != 'auth') {
        helper.throwApiError(res, 102, 'Login required.');
        return;
    }
    req.session.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log(req.session);
/*    if(req.session.user.id != 14){
	var fs = require('fs');
	var son = fs.readFileSync('what.json');
	console.log(son);
	res.end(son);
	return;	
    }*/
    next();
});

/*
 
 */


var r = require('./routes')
    .setUpRoutes(config.versions);

/* Setting up routes */

var routes, prefix;
for (var router in r.routers) {
    router = r.routers[router];
    routes = router.routes;
    for (var route in routes) {
        route = routes[route];
        app[route.method](router.prefix + route.path, router.controllers[route.controller][route.action]);
    }
}

//app.use(app.router);

/* Development settings */

// app.configure('development', function() {
//     app.use(express.errorHandler({
//         dumpExceptions: true,
//         showStack: true
//     }));
// });

// app.configure('production', function() {
//     app.use(express.errorHandler());
// });

/* Error handing fallback */

app.get('*', function(req, res) {
    helper.throwApiError(res, 101, 'Endpoint [' + app.locals.endPoint + '] not found.');
});

//http.globalAgent.maxSockets = 100;

http.createServer(app).listen(PORT,
    function() {
        console.log('Express server listening on port ' + PORT);
    }
);
