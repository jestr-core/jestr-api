module.exports = {
	postgres: {
		// postgres://jestr:uYbHp2eDE2-|k1v@localhost:5433/jestr
		protocol: 'postgres',
		username: 'jestr',
		password: 'uYbHp2eDE2-|k1v',
		host: 'localhost',
		port: 5433,
		database: 'jestr'
	},
	redis: {
		protocol: 'redis',
		username: '',
		password: '',
		host: 'localhost',
		port: 6379,
		database: 0
	},
	crypto: {
		cookieKey: '#"Tu9iHh3s!1s|8<\'5"10vuO19sQA,}2_{44En!,_R21TB&dI9c11_D4$1188HGs^[<)#1%\'g!Xd737@R.{11!0/1U2(76EHD359',
		// Hash-based Message Authentication Code
		HMACKey: 'S60[7I-6$?8$]!{24^QX%0rg6#39Ty6z#j5k"}2]:0?m=}J2J1V5n0:52uK_7/3L1_xO95 ~%:U3(+733z6(:<&A1;+@.][7=D~y',
		passwordSalt: 'Z}w+~)6//1_q!1<'
	},
	s3: {
		key: 'AKIAJK475ONJUNCILHKQ',
		secret: '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',
		bucket: 'zendeck',
		pathname: '/uploads/'
	},
	default: 'v1',
	versions: ['v1']
};