--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: jestr; Type: COMMENT; Schema: -; Owner: jestr
--

COMMENT ON DATABASE jestr IS 'Jestr main db, hstore used.';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = public, pg_catalog;

--
-- Name: notif_type; Type: TYPE; Schema: public; Owner: jestr
--

CREATE TYPE notif_type AS ENUM (
    'friend_request',
    'friend_request_confirmation',
    'new_private_message',
    'user_commented_on_post',
    'user_cooled_on_post'
);


ALTER TYPE public.notif_type OWNER TO jestr;

--
-- Name: user_preview; Type: TYPE; Schema: public; Owner: jestr
--

CREATE TYPE user_preview AS (
	id bigint,
	username character varying(20)
);


ALTER TYPE public.user_preview OWNER TO jestr;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comment; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE comment (
    id bigint NOT NULL,
    post_id bigint,
    user_id bigint,
    message text,
    date bigint,
    deleted boolean DEFAULT false
);


ALTER TABLE public.comment OWNER TO jestr;

--
-- Name: comment_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comment_id_seq OWNER TO jestr;

--
-- Name: comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE comment_id_seq OWNED BY comment.id;


--
-- Name: cool; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE cool (
    id bigint NOT NULL,
    user_id bigint,
    post_id bigint,
    date bigint,
    deleted boolean DEFAULT false
);


ALTER TABLE public.cool OWNER TO jestr;

--
-- Name: cool_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE cool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cool_id_seq OWNER TO jestr;

--
-- Name: cool_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE cool_id_seq OWNED BY cool.id;


--
-- Name: hash; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE hash (
    id bigint NOT NULL,
    post_id bigint,
    hash character varying(60)
);


ALTER TABLE public.hash OWNER TO jestr;

--
-- Name: hash_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE hash_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hash_id_seq OWNER TO jestr;

--
-- Name: hash_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE hash_id_seq OWNED BY hash.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE notification (
    id bigint NOT NULL,
    user_id bigint,
    from_id bigint,
    cause notif_type,
    related_object hstore,
    confirmed boolean DEFAULT false,
    date bigint,
    deleted boolean DEFAULT false
);


ALTER TABLE public.notification OWNER TO jestr;

--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_id_seq OWNER TO jestr;

--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE notification_id_seq OWNED BY notification.id;


--
-- Name: post; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE post (
    id bigint NOT NULL,
    user_id bigint,
    location hstore,
    images hstore,
    details hstore,
    comments hstore,
    likes hstore,
    hashtags hstore,
    user_relation_id bigint,
    date bigint,
    trending integer,
    deleted boolean DEFAULT false
);


ALTER TABLE public.post OWNER TO jestr;

--
-- Name: post_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_id_seq OWNER TO jestr;

--
-- Name: post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE post_id_seq OWNED BY post.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE "user" (
    id bigint NOT NULL,
    username character varying(20) NOT NULL,
    profile hstore,
    email character varying(120),
    location boolean DEFAULT false,
    private boolean DEFAULT false,
    "regDate" bigint,
    "lastLogin" bigint,
    "lastUpdate" bigint,
    deleted boolean DEFAULT false
);


ALTER TABLE public."user" OWNER TO jestr;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO jestr;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: user_relation; Type: TABLE; Schema: public; Owner: jestr; Tablespace: 
--

CREATE TABLE user_relation (
    id bigint NOT NULL,
    user_id bigint,
    friend_id bigint,
    date bigint,
    deleted boolean DEFAULT false
);


ALTER TABLE public.user_relation OWNER TO jestr;

--
-- Name: user_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: jestr
--

CREATE SEQUENCE user_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_relation_id_seq OWNER TO jestr;

--
-- Name: user_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jestr
--

ALTER SEQUENCE user_relation_id_seq OWNED BY user_relation.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY comment ALTER COLUMN id SET DEFAULT nextval('comment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY cool ALTER COLUMN id SET DEFAULT nextval('cool_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY hash ALTER COLUMN id SET DEFAULT nextval('hash_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY notification ALTER COLUMN id SET DEFAULT nextval('notification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY post ALTER COLUMN id SET DEFAULT nextval('post_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY user_relation ALTER COLUMN id SET DEFAULT nextval('user_relation_id_seq'::regclass);


--
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY comment (id, post_id, user_id, message, date, deleted) FROM stdin;
\.


--
-- Name: comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('comment_id_seq', 1, false);


--
-- Data for Name: cool; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY cool (id, user_id, post_id, date, deleted) FROM stdin;
\.


--
-- Name: cool_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('cool_id_seq', 1, false);


--
-- Data for Name: hash; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY hash (id, post_id, hash) FROM stdin;
\.


--
-- Name: hash_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('hash_id_seq', 1, false);


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY notification (id, user_id, from_id, cause, related_object, confirmed, date, deleted) FROM stdin;
\.


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('notification_id_seq', 1, false);


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY post (id, user_id, location, images, details, comments, likes, hashtags, user_relation_id, date, trending, deleted) FROM stdin;
3	1	"pisti"=>"nope"	"pisti"=>"nope"	"pisti"=>"nope"	"pisti"=>"nope"	"pisti"=>"nope"	"pisti"=>"nope"	1	0	10	f
\.


--
-- Name: post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('post_id_seq', 3, true);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY "user" (id, username, profile, email, location, private, "regDate", "lastLogin", "lastUpdate", deleted) FROM stdin;
1	jestr	"about"=>"akármi", "gender"=>"male", "picture"=>"nope"	info@jestr.me	f	f	0	0	0	f
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('user_id_seq', 1, true);


--
-- Data for Name: user_relation; Type: TABLE DATA; Schema: public; Owner: jestr
--

COPY user_relation (id, user_id, friend_id, date, deleted) FROM stdin;
1	1	1	0	f
\.


--
-- Name: user_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jestr
--

SELECT pg_catalog.setval('user_relation_id_seq', 1, true);


--
-- Name: comment_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- Name: cool_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY cool
    ADD CONSTRAINT cool_pkey PRIMARY KEY (id);


--
-- Name: hash_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY hash
    ADD CONSTRAINT hash_pkey PRIMARY KEY (id);


--
-- Name: hash_post_id_hash_unique; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY hash
    ADD CONSTRAINT hash_post_id_hash_unique UNIQUE (post_id, hash);


--
-- Name: notification_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- Name: post_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: user_email_key; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY user_relation
    ADD CONSTRAINT user_relation_pkey PRIMARY KEY (id);


--
-- Name: user_username_unique; Type: CONSTRAINT; Schema: public; Owner: jestr; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_unique UNIQUE (username);


--
-- Name: comment_post_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX comment_post_id ON comment USING btree (post_id);


--
-- Name: comment_user_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX comment_user_id ON comment USING btree (user_id);


--
-- Name: cool_post_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX cool_post_id ON cool USING btree (post_id);


--
-- Name: cool_user_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX cool_user_id ON cool USING btree (user_id);


--
-- Name: hash_hash; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX hash_hash ON hash USING hash (hash);


--
-- Name: notification_from_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX notification_from_id ON notification USING btree (from_id);


--
-- Name: notification_user_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX notification_user_id ON notification USING btree (user_id);


--
-- Name: post_trending; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX post_trending ON post USING btree (trending);


--
-- Name: post_user_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX post_user_id ON post USING btree (user_id);


--
-- Name: post_user_relation_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX post_user_relation_id ON post USING btree (user_relation_id);


--
-- Name: user_relation_friend_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX user_relation_friend_id ON user_relation USING btree (friend_id);


--
-- Name: user_relation_user_id; Type: INDEX; Schema: public; Owner: jestr; Tablespace: 
--

CREATE INDEX user_relation_user_id ON user_relation USING btree (user_id);


--
-- Name: comment_post_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_post_relation FOREIGN KEY (post_id) REFERENCES post(id);


--
-- Name: comment_user_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_user_relation FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: hash_post_id; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY hash
    ADD CONSTRAINT hash_post_id FOREIGN KEY (post_id) REFERENCES post(id);


--
-- Name: notification_from_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_from_relation FOREIGN KEY (from_id) REFERENCES "user"(id);


--
-- Name: notification_user_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_user_relation FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: post_user_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY post
    ADD CONSTRAINT post_user_relation FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: post_user_relation_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY post
    ADD CONSTRAINT post_user_relation_relation FOREIGN KEY (user_relation_id) REFERENCES user_relation(id);


--
-- Name: user_relation_friend_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY user_relation
    ADD CONSTRAINT user_relation_friend_relation FOREIGN KEY (friend_id) REFERENCES "user"(id);


--
-- Name: user_relation_user_relation; Type: FK CONSTRAINT; Schema: public; Owner: jestr
--

ALTER TABLE ONLY user_relation
    ADD CONSTRAINT user_relation_user_relation FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO jestr;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

