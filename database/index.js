/**
 * Wrapper for PostgreSQL 9.3
 * @author Árpád Kiss
 */

// to do: store insert/update results

var pg = require('pg');
var helper = require('../helper');

module.exports = (function() {
	var _connectionString = "";
	var _jsonPrefix = 'SELECT array_to_json(array_agg(row_to_json("row".*))) AS data FROM ( ';
	var _jsonSuffix = ') "row";';
	var _encapsulateToJsonQuery = function(query) {
		return _jsonPrefix + query + _jsonSuffix;
	};
	var _client = {};
	var _query = function(query, cb) {
		console.log(query);
		_client.query(query, function(err, result) {
			if (!cb) return;
			if (err) {
				cb(err);
				return;
			}
			cb(err, result.rows);
		});
	};
	return {
		init: function(cString) {
			_connectionString = cString;
			pg.connect(_connectionString, function(err, client) {
				if (err) {
					client.end();
					console.error('error fetching client from pool', err);
					cb(err);
					return;
				}
				_client = client;
			});
			return this;
		},
		query: function(query, params, cb) {
			if (!query) {
				cb('query is missing.');
			}
			if(typeof params == 'function') {
				cb = params;
			} else {
				for (var p in params) {
					query = query.replace(new RegExp('{' + p + '}','g'), params[p]);
				}
			}
			_query(query, cb);
		},
		get: function(what, from, where, pagination, cb) {
			var querySuffix = "";
			if (typeof(pagination) === "function") {
				cb = pagination;
			} else {
				querySuffix += (pagination.limit) ? " LIMIT " + pagination.limit : "";
				querySuffix += (querySuffix.length > 0 && pagination.page) ? " OFFSET " + (pagination.page * pagination.limit) : "";
				querySuffix = (pagination.orderby) ? "ORDER BY " + pagination.orderby + " " + querySuffix : querySuffix;
			}
			var w = "";
			if(typeof where == 'object') {
				for (var key in where) {
					if( typeof where[key] != "object" ) {
						w += '"' + key + '" = \'' + where[key] + '\' AND ';
					} else {
						w += '"' + key + '" ' + where[key].operand + ' ' + where[key].value + ' AND ';
					}
				}
				w = w.substr(0, w.length - 5);

				w = (w.length > 0) ? 'WHERE ' + w : '';
			} else {
				w = where;
			}
			what = (what[0] != "*") ? '"' + what.join('","') + '"' : "*";

			var query = 'SELECT ' + what + ' FROM "' + from + '" ' + w + querySuffix;
			_query(_encapsulateToJsonQuery(query), function(err, result) {
				if (err) {
					cb(err);
					return;
				}
				cb(err, result[0].data);
			});
		},
		set: function(table, fields, where, cb) {
			// prefetch
			var keys = [];
			for (var key in fields) {
				keys.push(key);
			}
			this.getRow(keys, table, where, function(err, result) {
				if (err) {
					cb(err);
					return;
				}

				fields = helper.mergeJson(result, fields);

				var query = 'UPDATE "' + table + '" SET ';
				var k = [];
				var d = "",
					value = "",
					key;
				for (key in fields) {
					value = typeof fields[key] === 'object' ? '\'' + JSON.stringify(fields[key]) + '\'' : '\'' + fields[key] + '\'';
					d += '"' + key + '" =  ' + value + ', ';
					k.push(key);
				}
				d = d.substr(0, d.length - 2);

				var w = "";
				for (key in where) {
					w += '"' + key + '" = \'' + where[key] + '\' AND ';
				}
				w = w.substr(0, w.length - 5);

				query += d + ' WHERE ' + w + ' RETURNING "id"';
				query += ',"' + k.join('","') + '"';
				_query(query, cb);
			});
		},
		add: function(table, fields, cb) {
			var query = 'INSERT INTO "' + table + '" (';
			var k = [],
				v = [],
				value = "";
			for (var key in fields) {
				value = typeof fields[key] === 'object' ? JSON.stringify(fields[key]) : fields[key];
				k.push(key);
				v.push("'" + value + "'");
			}
			query += '"' + k.join('","') + '") VALUES (' + v.join(',') + ') RETURNING "id"';
			query += ',"' + k.join('","') + '"';
			_query(query, cb);
		},
		getRow: function(what, from, where, cb) {
			var w = "";
			if(typeof where == 'object') {
				for (var key in where) {
					if( typeof where[key] != "object" ) {
						w += '"' + key + '" = \'' + where[key] + '\' AND ';
					} else {
						w += '"' + key + '" ' + where[key].operand + ' ' + where[key].value + ' AND ';
					}
				}
				w = w.substr(0, w.length - 5);
			} else {
				w = where;
			}

			what = (what[0] != "*") ? '"' + what.join('","') + '"' : "*";

			var query = 'SELECT ' + what + ' FROM "' + from + '" WHERE ' + w + ' LIMIT 1';
			_query(_encapsulateToJsonQuery(query), function(err, result) {
				if (!err && result[0].data !== null) {
					result = result[0].data[0];
					cb(err, result);
					return;
				}
				cb(true);
			});
		}
	};
})();